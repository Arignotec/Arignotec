generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}

// Database model for Schools, Classes, Teachers, Subjects, and Students

// Definition of Schools
model School {
  id            String        @id @default(uuid())
  name          String
  schoolClasses SchoolClass[]
  teacher       Teacher[]
}

// Definition of Classes
model SchoolClass {
  id                   String                @id @default(uuid())
  schoolClassName      String
  students             Student[]
  classSubjectTeachers ClassSubjectTeacher[]
  School               School                @relation(fields: [schoolId], references: [id])
  schoolId             String
}

// New model to track which subject each teacher teaches in a class
model ClassSubjectTeacher {
  id          String      @id @default(uuid())
  schoolClass SchoolClass @relation(fields: [classId], references: [id])
  classId     String
  subject     Subject     @relation(fields: [subjectId], references: [id])
  subjectId   String
  teacher     Teacher     @relation(fields: [teacherId], references: [id])
  teacherId   String

  @@unique([classId, subjectId, teacherId])
}

// Definition of Teachers
model Teacher {
  id                  String                @id @default(uuid())
  firstName           String
  lastName            String
  email               String                @unique
  password            String
  school              School                @relation(fields: [schoolId], references: [id])
  schoolId            String
  ClassSubjectTeacher ClassSubjectTeacher[]
}

// Definition of Subjects
model Subject {
  id                  String                @id @default(uuid())
  subjectName         String
  studentGrades       StudentGrade[]
  ClassSubjectTeacher ClassSubjectTeacher[]
}

// Definition of Students
model Student {
  id            String         @id @default(uuid())
  firstName     String
  lastName      String
  email         String         @unique
  password      String
  class         SchoolClass    @relation(fields: [classId], references: [id])
  classId       String
  studentGrades StudentGrade[]
}

// Definition of Grades
model StudentGrade {
  id        String  @id @default(uuid())
  student   Student @relation(fields: [studentId], references: [id])
  studentId String
  subject   Subject @relation(fields: [subjectId], references: [id])
  subjectId String
  grade     String
  comment   String
}
