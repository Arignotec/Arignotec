/*
  Warnings:

  - You are about to drop the column `value` on the `grade` table. All the data in the column will be lost.
  - You are about to drop the column `schoolName` on the `school` table. All the data in the column will be lost.
  - You are about to drop the column `email` on the `student` table. All the data in the column will be lost.
  - You are about to drop the column `password` on the `student` table. All the data in the column will be lost.
  - You are about to drop the column `email` on the `teacher` table. All the data in the column will be lost.
  - You are about to drop the column `password` on the `teacher` table. All the data in the column will be lost.
  - You are about to drop the `_schoolclasstoteacher` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_subjecttoteacher` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `schoolclass` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `grade` to the `Grade` table without a default value. This is not possible if the table is not empty.
  - Added the required column `subjectId` to the `Grade` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `School` table without a default value. This is not possible if the table is not empty.
  - Added the required column `classId` to the `Subject` table without a default value. This is not possible if the table is not empty.
  - Added the required column `teacherId` to the `Subject` table without a default value. This is not possible if the table is not empty.
  - Made the column `schoolId` on table `teacher` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE `_schoolclasstoteacher` DROP FOREIGN KEY `_SchoolClassToTeacher_A_fkey`;

-- DropForeignKey
ALTER TABLE `_schoolclasstoteacher` DROP FOREIGN KEY `_SchoolClassToTeacher_B_fkey`;

-- DropForeignKey
ALTER TABLE `_subjecttoteacher` DROP FOREIGN KEY `_SubjectToTeacher_A_fkey`;

-- DropForeignKey
ALTER TABLE `_subjecttoteacher` DROP FOREIGN KEY `_SubjectToTeacher_B_fkey`;

-- DropForeignKey
ALTER TABLE `schoolclass` DROP FOREIGN KEY `SchoolClass_schoolId_fkey`;

-- DropForeignKey
ALTER TABLE `student` DROP FOREIGN KEY `Student_classId_fkey`;

-- DropForeignKey
ALTER TABLE `teacher` DROP FOREIGN KEY `Teacher_schoolId_fkey`;

-- DropIndex
DROP INDEX `Student_email_key` ON `student`;

-- DropIndex
DROP INDEX `Teacher_email_key` ON `teacher`;

-- AlterTable
ALTER TABLE `grade` DROP COLUMN `value`,
    ADD COLUMN `grade` INTEGER NOT NULL,
    ADD COLUMN `subjectId` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `school` DROP COLUMN `schoolName`,
    ADD COLUMN `name` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `student` DROP COLUMN `email`,
    DROP COLUMN `password`;

-- AlterTable
ALTER TABLE `subject` ADD COLUMN `classId` VARCHAR(191) NOT NULL,
    ADD COLUMN `teacherId` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `teacher` DROP COLUMN `email`,
    DROP COLUMN `password`,
    MODIFY `schoolId` VARCHAR(191) NOT NULL;

-- DropTable
DROP TABLE `_schoolclasstoteacher`;

-- DropTable
DROP TABLE `_subjecttoteacher`;

-- DropTable
DROP TABLE `schoolclass`;

-- CreateTable
CREATE TABLE `Class` (
    `id` VARCHAR(191) NOT NULL,
    `name` VARCHAR(191) NOT NULL,
    `schoolId` VARCHAR(191) NOT NULL,
    `teacherId` VARCHAR(191) NOT NULL,

    INDEX `teacherId_index`(`teacherId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Teacher` ADD CONSTRAINT `Teacher_schoolId_fkey` FOREIGN KEY (`schoolId`) REFERENCES `School`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Class` ADD CONSTRAINT `Class_schoolId_fkey` FOREIGN KEY (`schoolId`) REFERENCES `School`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Class` ADD CONSTRAINT `Class_teacherId_fkey` FOREIGN KEY (`teacherId`) REFERENCES `Teacher`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Student` ADD CONSTRAINT `Student_classId_fkey` FOREIGN KEY (`classId`) REFERENCES `Class`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Subject` ADD CONSTRAINT `Subject_teacherId_fkey` FOREIGN KEY (`teacherId`) REFERENCES `Teacher`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Subject` ADD CONSTRAINT `Subject_classId_fkey` FOREIGN KEY (`classId`) REFERENCES `Class`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Grade` ADD CONSTRAINT `Grade_subjectId_fkey` FOREIGN KEY (`subjectId`) REFERENCES `Subject`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
