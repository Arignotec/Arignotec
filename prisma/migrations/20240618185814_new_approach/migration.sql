/*
  Warnings:

  - You are about to drop the column `subjectId` on the `grade` table. All the data in the column will be lost.
  - You are about to drop the column `name` on the `school` table. All the data in the column will be lost.
  - You are about to drop the column `name` on the `schoolclass` table. All the data in the column will be lost.
  - You are about to drop the column `classTeacherId` on the `subject` table. All the data in the column will be lost.
  - You are about to drop the column `teacherId` on the `subject` table. All the data in the column will be lost.
  - You are about to drop the `classteacher` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `test` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `testscore` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `schoolName` to the `School` table without a default value. This is not possible if the table is not empty.
  - Added the required column `className` to the `SchoolClass` table without a default value. This is not possible if the table is not empty.
  - Made the column `formTeacherId` on table `schoolclass` required. This step will fail if there are existing NULL values in that column.
  - Made the column `schoolId` on table `schoolclass` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE `classteacher` DROP FOREIGN KEY `ClassTeacher_classId_fkey`;

-- DropForeignKey
ALTER TABLE `classteacher` DROP FOREIGN KEY `ClassTeacher_teacherId_fkey`;

-- DropForeignKey
ALTER TABLE `grade` DROP FOREIGN KEY `Grade_subjectId_fkey`;

-- DropForeignKey
ALTER TABLE `schoolclass` DROP FOREIGN KEY `SchoolClass_formTeacherId_fkey`;

-- DropForeignKey
ALTER TABLE `schoolclass` DROP FOREIGN KEY `SchoolClass_schoolId_fkey`;

-- DropForeignKey
ALTER TABLE `subject` DROP FOREIGN KEY `Subject_classTeacherId_fkey`;

-- DropForeignKey
ALTER TABLE `subject` DROP FOREIGN KEY `Subject_teacherId_fkey`;

-- DropForeignKey
ALTER TABLE `test` DROP FOREIGN KEY `Test_classId_fkey`;

-- DropForeignKey
ALTER TABLE `test` DROP FOREIGN KEY `Test_subjectId_fkey`;

-- DropForeignKey
ALTER TABLE `test` DROP FOREIGN KEY `Test_teacherId_fkey`;

-- DropForeignKey
ALTER TABLE `testscore` DROP FOREIGN KEY `TestScore_studentId_fkey`;

-- DropForeignKey
ALTER TABLE `testscore` DROP FOREIGN KEY `TestScore_testId_fkey`;

-- AlterTable
ALTER TABLE `grade` DROP COLUMN `subjectId`;

-- AlterTable
ALTER TABLE `school` DROP COLUMN `name`,
    ADD COLUMN `schoolName` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `schoolclass` DROP COLUMN `name`,
    ADD COLUMN `className` VARCHAR(191) NOT NULL,
    MODIFY `formTeacherId` VARCHAR(191) NOT NULL,
    MODIFY `schoolId` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `subject` DROP COLUMN `classTeacherId`,
    DROP COLUMN `teacherId`;

-- DropTable
DROP TABLE `classteacher`;

-- DropTable
DROP TABLE `test`;

-- DropTable
DROP TABLE `testscore`;

-- CreateTable
CREATE TABLE `_SchoolClassToTeacher` (
    `A` VARCHAR(191) NOT NULL,
    `B` VARCHAR(191) NOT NULL,

    UNIQUE INDEX `_SchoolClassToTeacher_AB_unique`(`A`, `B`),
    INDEX `_SchoolClassToTeacher_B_index`(`B`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `_SubjectToTeacher` (
    `A` VARCHAR(191) NOT NULL,
    `B` VARCHAR(191) NOT NULL,

    UNIQUE INDEX `_SubjectToTeacher_AB_unique`(`A`, `B`),
    INDEX `_SubjectToTeacher_B_index`(`B`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `SchoolClass` ADD CONSTRAINT `SchoolClass_schoolId_fkey` FOREIGN KEY (`schoolId`) REFERENCES `School`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_SchoolClassToTeacher` ADD CONSTRAINT `_SchoolClassToTeacher_A_fkey` FOREIGN KEY (`A`) REFERENCES `SchoolClass`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_SchoolClassToTeacher` ADD CONSTRAINT `_SchoolClassToTeacher_B_fkey` FOREIGN KEY (`B`) REFERENCES `Teacher`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_SubjectToTeacher` ADD CONSTRAINT `_SubjectToTeacher_A_fkey` FOREIGN KEY (`A`) REFERENCES `Subject`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_SubjectToTeacher` ADD CONSTRAINT `_SubjectToTeacher_B_fkey` FOREIGN KEY (`B`) REFERENCES `Teacher`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
