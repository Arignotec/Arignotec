/*
  Warnings:

  - The primary key for the `grade` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `grade` on the `grade` table. All the data in the column will be lost.
  - You are about to drop the column `student_id` on the `grade` table. All the data in the column will be lost.
  - You are about to drop the column `subject_id` on the `grade` table. All the data in the column will be lost.
  - You are about to drop the column `year_id` on the `grade` table. All the data in the column will be lost.
  - The primary key for the `school` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `school_name` on the `school` table. All the data in the column will be lost.
  - The primary key for the `student` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `class_id` on the `student` table. All the data in the column will be lost.
  - You are about to drop the column `first_name` on the `student` table. All the data in the column will be lost.
  - You are about to drop the column `last_name` on the `student` table. All the data in the column will be lost.
  - The primary key for the `subject` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `teacher` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `first_name` on the `teacher` table. All the data in the column will be lost.
  - You are about to drop the column `last_name` on the `teacher` table. All the data in the column will be lost.
  - The primary key for the `test` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `note` on the `test` table. All the data in the column will be lost.
  - You are about to drop the column `teacher_subject_id` on the `test` table. All the data in the column will be lost.
  - You are about to drop the `class` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `classsubject` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `classyear` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `teacherclass` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `teacherschool` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `teachersubject` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `year` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[email]` on the table `Student` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[email]` on the table `Teacher` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `studentId` to the `Grade` table without a default value. This is not possible if the table is not empty.
  - Added the required column `subjectId` to the `Grade` table without a default value. This is not possible if the table is not empty.
  - Added the required column `value` to the `Grade` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `School` table without a default value. This is not possible if the table is not empty.
  - Added the required column `classId` to the `Student` table without a default value. This is not possible if the table is not empty.
  - Added the required column `firstName` to the `Student` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lastName` to the `Student` table without a default value. This is not possible if the table is not empty.
  - Added the required column `password` to the `Student` table without a default value. This is not possible if the table is not empty.
  - Made the column `email` on table `student` required. This step will fail if there are existing NULL values in that column.
  - Made the column `name` on table `subject` required. This step will fail if there are existing NULL values in that column.
  - Added the required column `firstName` to the `Teacher` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lastName` to the `Teacher` table without a default value. This is not possible if the table is not empty.
  - Added the required column `password` to the `Teacher` table without a default value. This is not possible if the table is not empty.
  - Made the column `email` on table `teacher` required. This step will fail if there are existing NULL values in that column.
  - Added the required column `classId` to the `Test` table without a default value. This is not possible if the table is not empty.
  - Added the required column `comment` to the `Test` table without a default value. This is not possible if the table is not empty.
  - Added the required column `subjectId` to the `Test` table without a default value. This is not possible if the table is not empty.
  - Added the required column `teacherId` to the `Test` table without a default value. This is not possible if the table is not empty.
  - Made the column `name` on table `test` required. This step will fail if there are existing NULL values in that column.
  - Made the column `date` on table `test` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE `classsubject` DROP FOREIGN KEY `classsubject_ibfk_1`;

-- DropForeignKey
ALTER TABLE `classsubject` DROP FOREIGN KEY `classsubject_ibfk_2`;

-- DropForeignKey
ALTER TABLE `classyear` DROP FOREIGN KEY `classyear_ibfk_1`;

-- DropForeignKey
ALTER TABLE `classyear` DROP FOREIGN KEY `classyear_ibfk_2`;

-- DropForeignKey
ALTER TABLE `grade` DROP FOREIGN KEY `grade_ibfk_1`;

-- DropForeignKey
ALTER TABLE `grade` DROP FOREIGN KEY `grade_ibfk_2`;

-- DropForeignKey
ALTER TABLE `grade` DROP FOREIGN KEY `grade_ibfk_3`;

-- DropForeignKey
ALTER TABLE `student` DROP FOREIGN KEY `student_ibfk_1`;

-- DropForeignKey
ALTER TABLE `teacherclass` DROP FOREIGN KEY `teacherclass_ibfk_1`;

-- DropForeignKey
ALTER TABLE `teacherclass` DROP FOREIGN KEY `teacherclass_ibfk_2`;

-- DropForeignKey
ALTER TABLE `teacherschool` DROP FOREIGN KEY `teacherschool_ibfk_1`;

-- DropForeignKey
ALTER TABLE `teacherschool` DROP FOREIGN KEY `teacherschool_ibfk_2`;

-- DropForeignKey
ALTER TABLE `teachersubject` DROP FOREIGN KEY `teachersubject_ibfk_1`;

-- DropForeignKey
ALTER TABLE `teachersubject` DROP FOREIGN KEY `teachersubject_ibfk_2`;

-- DropForeignKey
ALTER TABLE `test` DROP FOREIGN KEY `test_ibfk_1`;

-- AlterTable
ALTER TABLE `grade` DROP PRIMARY KEY,
    DROP COLUMN `grade`,
    DROP COLUMN `student_id`,
    DROP COLUMN `subject_id`,
    DROP COLUMN `year_id`,
    ADD COLUMN `studentId` VARCHAR(191) NOT NULL,
    ADD COLUMN `subjectId` VARCHAR(191) NOT NULL,
    ADD COLUMN `value` INTEGER NOT NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `school` DROP PRIMARY KEY,
    DROP COLUMN `school_name`,
    ADD COLUMN `name` VARCHAR(191) NOT NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `student` DROP PRIMARY KEY,
    DROP COLUMN `class_id`,
    DROP COLUMN `first_name`,
    DROP COLUMN `last_name`,
    ADD COLUMN `classId` VARCHAR(191) NOT NULL,
    ADD COLUMN `firstName` VARCHAR(191) NOT NULL,
    ADD COLUMN `lastName` VARCHAR(191) NOT NULL,
    ADD COLUMN `password` VARCHAR(191) NOT NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    MODIFY `email` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `subject` DROP PRIMARY KEY,
    ADD COLUMN `classTeacherId` VARCHAR(191) NULL,
    ADD COLUMN `teacherId` VARCHAR(191) NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    MODIFY `name` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `teacher` DROP PRIMARY KEY,
    DROP COLUMN `first_name`,
    DROP COLUMN `last_name`,
    ADD COLUMN `firstName` VARCHAR(191) NOT NULL,
    ADD COLUMN `lastName` VARCHAR(191) NOT NULL,
    ADD COLUMN `password` VARCHAR(191) NOT NULL,
    ADD COLUMN `schoolId` VARCHAR(191) NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    MODIFY `email` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `test` DROP PRIMARY KEY,
    DROP COLUMN `note`,
    DROP COLUMN `teacher_subject_id`,
    ADD COLUMN `classId` VARCHAR(191) NOT NULL,
    ADD COLUMN `comment` VARCHAR(191) NOT NULL,
    ADD COLUMN `subjectId` VARCHAR(191) NOT NULL,
    ADD COLUMN `teacherId` VARCHAR(191) NOT NULL,
    MODIFY `id` VARCHAR(191) NOT NULL,
    MODIFY `name` VARCHAR(191) NOT NULL,
    MODIFY `date` DATETIME(3) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- DropTable
DROP TABLE `class`;

-- DropTable
DROP TABLE `classsubject`;

-- DropTable
DROP TABLE `classyear`;

-- DropTable
DROP TABLE `teacherclass`;

-- DropTable
DROP TABLE `teacherschool`;

-- DropTable
DROP TABLE `teachersubject`;

-- DropTable
DROP TABLE `year`;

-- CreateTable
CREATE TABLE `SchoolClass` (
    `id` VARCHAR(191) NOT NULL,
    `name` VARCHAR(191) NOT NULL,
    `formTeacherId` VARCHAR(191) NULL,
    `schoolId` VARCHAR(191) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `TestScore` (
    `id` VARCHAR(191) NOT NULL,
    `score` INTEGER NOT NULL,
    `studentId` VARCHAR(191) NOT NULL,
    `testId` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ClassTeacher` (
    `id` VARCHAR(191) NOT NULL,
    `classId` VARCHAR(191) NOT NULL,
    `teacherId` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `Student_email_key` ON `Student`(`email`);

-- CreateIndex
CREATE UNIQUE INDEX `Teacher_email_key` ON `Teacher`(`email`);

-- AddForeignKey
ALTER TABLE `SchoolClass` ADD CONSTRAINT `SchoolClass_formTeacherId_fkey` FOREIGN KEY (`formTeacherId`) REFERENCES `Teacher`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `SchoolClass` ADD CONSTRAINT `SchoolClass_schoolId_fkey` FOREIGN KEY (`schoolId`) REFERENCES `School`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Teacher` ADD CONSTRAINT `Teacher_schoolId_fkey` FOREIGN KEY (`schoolId`) REFERENCES `School`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Student` ADD CONSTRAINT `Student_classId_fkey` FOREIGN KEY (`classId`) REFERENCES `SchoolClass`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Subject` ADD CONSTRAINT `Subject_teacherId_fkey` FOREIGN KEY (`teacherId`) REFERENCES `Teacher`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Subject` ADD CONSTRAINT `Subject_classTeacherId_fkey` FOREIGN KEY (`classTeacherId`) REFERENCES `ClassTeacher`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Grade` ADD CONSTRAINT `Grade_studentId_fkey` FOREIGN KEY (`studentId`) REFERENCES `Student`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Grade` ADD CONSTRAINT `Grade_subjectId_fkey` FOREIGN KEY (`subjectId`) REFERENCES `Subject`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Test` ADD CONSTRAINT `Test_teacherId_fkey` FOREIGN KEY (`teacherId`) REFERENCES `Teacher`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Test` ADD CONSTRAINT `Test_classId_fkey` FOREIGN KEY (`classId`) REFERENCES `SchoolClass`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Test` ADD CONSTRAINT `Test_subjectId_fkey` FOREIGN KEY (`subjectId`) REFERENCES `Subject`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `TestScore` ADD CONSTRAINT `TestScore_studentId_fkey` FOREIGN KEY (`studentId`) REFERENCES `Student`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `TestScore` ADD CONSTRAINT `TestScore_testId_fkey` FOREIGN KEY (`testId`) REFERENCES `Test`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ClassTeacher` ADD CONSTRAINT `ClassTeacher_classId_fkey` FOREIGN KEY (`classId`) REFERENCES `SchoolClass`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ClassTeacher` ADD CONSTRAINT `ClassTeacher_teacherId_fkey` FOREIGN KEY (`teacherId`) REFERENCES `Teacher`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
