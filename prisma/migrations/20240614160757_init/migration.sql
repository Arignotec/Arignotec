-- CreateTable
CREATE TABLE `class` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `class_name` VARCHAR(50) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `classsubject` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `class_id` INTEGER NULL,
    `subject_id` INTEGER NULL,

    INDEX `class_id`(`class_id`),
    INDEX `subject_id`(`subject_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `classyear` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `class_id` INTEGER NULL,
    `year_id` INTEGER NULL,

    INDEX `class_id`(`class_id`),
    INDEX `year_id`(`year_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `grade` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `grade` DECIMAL(2, 1) NULL,
    `student_id` INTEGER NOT NULL,
    `subject_id` INTEGER NOT NULL,
    `year_id` INTEGER NOT NULL,

    INDEX `student_id`(`student_id`),
    INDEX `subject_id`(`subject_id`),
    INDEX `year_id`(`year_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `school` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `school_name` VARCHAR(100) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `student` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(50) NULL,
    `last_name` VARCHAR(50) NULL,
    `email` VARCHAR(100) NULL,
    `class_id` INTEGER NULL,

    INDEX `class_id`(`class_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `subject` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `teacher` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(50) NULL,
    `last_name` VARCHAR(50) NULL,
    `email` VARCHAR(100) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `teacherclass` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `teacher_id` INTEGER NULL,
    `class_id` INTEGER NULL,

    INDEX `class_id`(`class_id`),
    INDEX `teacher_id`(`teacher_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `teacherschool` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `teacher_id` INTEGER NULL,
    `school_id` INTEGER NULL,

    INDEX `school_id`(`school_id`),
    INDEX `teacher_id`(`teacher_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `teachersubject` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `teacher_id` INTEGER NULL,
    `subject_id` INTEGER NULL,

    INDEX `subject_id`(`subject_id`),
    INDEX `teacher_id`(`teacher_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `test` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL,
    `date` DATE NULL,
    `note` TEXT NULL,
    `teacher_subject_id` INTEGER NULL,

    INDEX `teacher_subject_id`(`teacher_subject_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `year` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `year` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `classsubject` ADD CONSTRAINT `classsubject_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `classsubject` ADD CONSTRAINT `classsubject_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subject`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `classyear` ADD CONSTRAINT `classyear_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `classyear` ADD CONSTRAINT `classyear_ibfk_2` FOREIGN KEY (`year_id`) REFERENCES `year`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `grade` ADD CONSTRAINT `grade_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `grade` ADD CONSTRAINT `grade_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subject`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `grade` ADD CONSTRAINT `grade_ibfk_3` FOREIGN KEY (`year_id`) REFERENCES `year`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `student` ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teacherclass` ADD CONSTRAINT `teacherclass_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teacherclass` ADD CONSTRAINT `teacherclass_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teacherschool` ADD CONSTRAINT `teacherschool_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teacherschool` ADD CONSTRAINT `teacherschool_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `school`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teachersubject` ADD CONSTRAINT `teachersubject_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `teachersubject` ADD CONSTRAINT `teachersubject_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subject`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `test` ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`teacher_subject_id`) REFERENCES `teachersubject`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
