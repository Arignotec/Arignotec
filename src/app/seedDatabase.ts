import {
    PrismaClient,
    School,
} from '@prisma/client';

const prisma = new PrismaClient();

// Interface for inserting School data
interface SchoolCreateInput {
    name: string;
}

// Interface for inserting SchoolClass data
interface SchoolClassCreateInput {
    id: string;
    schoolClassName: string;
    schoolId: string;
}

// Interface for inserting ClassSubjectTeacher data
interface ClassSubjectTeacherCreateInput {
    classId: string;
    subjectId: string;
    teacherId: string;
}

// Interface for inserting Teacher data
interface TeacherCreateInput {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    schoolId: string;
}

// Interface for inserting Subject data
interface SubjectCreateInput {
    id: string;
    subjectName: string;
}

// Interface for inserting Student data
interface StudentCreateInput {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    classId: string;
}

// Interface for inserting StudentGrade data
interface StudentGradeCreateInput {
    studentId: string;
    subjectId: string;
    grade: string;
    comment: string;
}

// Method to generate UUID
function generateUUID(): string {

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = (Math.random() * 16) | 0;
        const v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}

//generate hashes
const bcrypt = require('bcryptjs');

async function hashString(plainText: string) {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashedString = await bcrypt.hash(plainText, salt);
        return hashedString;
    } catch (error) {
        console.error('Error hashing string:', error);
        throw error;
    }
}

async function main() {
    try {
        // Create School
        const schoolData: SchoolCreateInput = {
            name: 'HTL-Wels',
        };
        const createdSchool: School = await prisma.school.create({
            data: schoolData,
        });

        const subjectData: SubjectCreateInput[] = [
            {
                id: generateUUID(),
                subjectName: 'Softwareentwicklung',
            },
            {
                id: generateUUID(),
                subjectName: 'Medientechnik',
            },
            {
                id: generateUUID(),
                subjectName: 'Informationstechnische Projekte',
            },
        ];

        // Create an array of promises for creating each subject
        const subjectPromises = subjectData.map(subject =>
            prisma.subject.create({
                data: subject,
            })
        );
        // Wait for all promises to resolve
        const createdSubjects: SubjectCreateInput[] = await Promise.all(subjectPromises);

        // Create SchoolClasses
        const classesData: SchoolClassCreateInput[] = [
            {
                id: generateUUID(),
                schoolClassName: '3CHIT',
                schoolId: createdSchool.id,
            },
            {
                id: generateUUID(),
                schoolClassName: '4BHIT',
                schoolId: createdSchool.id,
            },
        ];

        // Create an array of promises for creating each class
        const classPromises = classesData.map(classData =>
            prisma.schoolClass.create({
                data: classData,
            })
        );


        const createdClasses: SchoolClassCreateInput[] = await Promise.all(classPromises);

        const teachersData = [
            {
                id: generateUUID(),
                firstName: 'Manfred',
                lastName: 'Widmann',
                email: 'manfred.widmann@htl-wels.at',
                password: await hashString("HTL12345"),
                schoolId: createdSchool.id,
            },
            {
                id: generateUUID(),
                firstName: 'Jane',
                lastName: 'Smith',
                email: 'jane.smith@htl-wels.at',
                password: await hashString("JanESecure"),
                schoolId: createdSchool.id,
            },
            {
                id: generateUUID(),
                firstName: 'Michael',
                lastName: 'Mayer',
                email: 'michael.mayer@htl-wels.at',
                password: await hashString("michaeLMayer"),
                schoolId: createdSchool.id,
            },
        ];

        // Create an array of promises for creating each teacher
        const teacherPromises = teachersData.map(teacherData =>
            prisma.teacher.create({
                data: teacherData,
            })
        );

        // Wait for all promises to resolve
        const createdTeachers:TeacherCreateInput[] = await Promise.all(teacherPromises);

        const classSubjectTeacherPromises = [
            {
                id: generateUUID(),
                classId: createdClasses[0].id,      //in welcher klasse
                subjectId: createdSubjects[0].id,   //welches Fach
                teacherId: createdTeachers[0].id    //von welchem Lehrer
            },
            {
                id: generateUUID(),
                classId: createdClasses[0].id,
                subjectId: createdSubjects[1].id,
                teacherId: createdTeachers[1].id
            },
            {
                id: generateUUID(),
                classId: createdClasses[0].id,
                subjectId: createdSubjects[2].id,
                teacherId: createdTeachers[0].id
            },
            {
                id: generateUUID(),
                classId: createdClasses[1].id,
                subjectId: createdSubjects[0].id,
                teacherId: createdTeachers[2].id
            },
            {
                id: generateUUID(),
                classId: createdClasses[1].id,
                subjectId: createdSubjects[1].id,
                teacherId: createdTeachers[1].id
            },
            {
                id: generateUUID(),
                classId: createdClasses[1].id,
                subjectId: createdSubjects[2].id,
                teacherId: createdTeachers[2].id
            },
        ].map(classSubjectTeacherData =>
            prisma.classSubjectTeacher.create({
                data: classSubjectTeacherData,
            })
        );

        // Wait for all promises to resolve
        const createdClassSubjectTeachers: ClassSubjectTeacherCreateInput[] = await Promise.all(classSubjectTeacherPromises);

        // Create Students
        const studentData: StudentCreateInput[] = [
            {
                id: generateUUID(),
                firstName: 'Lukas',
                lastName: 'Trausner',
                email: 'lukas.trausner@htl-wels.at',
                password: await hashString("Trausi2"),
                classId: createdClasses[0].id,
            },
            {
                id: generateUUID(),
                firstName: 'Eren',
                lastName: 'Turgut',
                email: 'eren.turgut@htl-wels.at',
                password: await hashString("Uhcakip10"),
                classId: createdClasses[0].id,
            },
            {
                id: generateUUID(),
                firstName: 'Daniel',
                lastName: 'Ueberbacher',
                email: 'daniel.ueberbacher@htl-wels.at',
                password: await hashString("SecUrePassWoRd.!"),
                classId: createdClasses[0].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student4',
                lastName: 'Doe',
                email: 'student4@htl-wels.at',
                password: await hashString("password4"),
                classId: createdClasses[0].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student5',
                lastName: 'Doe',
                email: 'student5@htl-wels.at',
                password: await hashString("password5"),
                classId: createdClasses[0].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student6',
                lastName: 'Doe',
                email: 'student6@htl-wels.at',
                password: await hashString("password6"),
                classId: createdClasses[1].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student7',
                lastName: 'Doe',
                email: 'student7@htl-wels.at',
                password: await hashString("password7"),
                classId: createdClasses[1].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student8',
                lastName: 'Doe',
                email: 'student8@htl-wels.at',
                password: await hashString("password8"),
                classId: createdClasses[1].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student9',
                lastName: 'Doe',
                email: 'student9@htl-wels.at',
                password: await hashString("password9"),
                classId: createdClasses[1].id,
            },
            {
                id: generateUUID(),
                firstName: 'Student10',
                lastName: 'Doe',
                email: 'student10@htl-wels.at',
                password: await hashString("password10"),
                classId: createdClasses[1].id,
            },
        ];

        // Assuming prisma is your Prisma client instance
        const students = await prisma.student.createMany({
            data: studentData,
        });

        // Create Student Grades
        const studentGradesData: StudentGradeCreateInput[] = [
            {
                studentId: studentData[0].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[0].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[0].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[1].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[1].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[1].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[2].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[2].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[2].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[3].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[3].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[3].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[4].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[4].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[4].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[5].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[5].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[5].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[6].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[6].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[6].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[7].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[7].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[7].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[8].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[8].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[8].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[9].id,
                subjectId: subjectData[0].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[9].id,
                subjectId: subjectData[1].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
            {
                studentId: studentData[9].id,
                subjectId: subjectData[2].id,
                grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                comment: '-',
            },
        ];

        await prisma.studentGrade.createMany({
            data: studentGradesData,
        });

        console.log('Database seeding completed successfully.');
    } catch (error) {
        console.error('Error seeding database:', error);
    } finally {
        await prisma.$disconnect();
    }
}

main();
