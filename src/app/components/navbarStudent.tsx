"use client";
import Link from "next/link";

export default function NavbarStudent() {
    return (
        <main>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="..">
                    <img src="/logo.png" alt="Logo" width="70"/>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link" href="/student">Noten</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" href="/student/tests">Tests</Link>
                        </li>
                    </ul>
                </div>
                <button className="btn btn-outline-secondary my-2 my-sm-0 mr-3" onClick={() => window.location.href = ".."}>Logout</button>
            </nav>

        </main>
    );
}