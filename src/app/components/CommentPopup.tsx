import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

interface GradeCommentPopupProps {
    comment: string;
    onSave?: (updatedComment: string) => void; // Optional: onSave-Funktion
    onClose: () => void;
    allowEdit?: boolean; // Optional: Erlaube Bearbeiten des Kommentars
}

const CommentPopup: React.FC<GradeCommentPopupProps> = ({ comment, onSave, onClose, allowEdit = true }) => {
    const [updatedComment, setUpdatedComment] = useState(comment);

    const handleSave = () => {
        if (onSave) {
            onSave(updatedComment);
        }
        onClose();
    };

    const handleClose = () => {
        onClose();
    };

    return (
        <Modal show={true} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Kommentar bearbeiten</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <Form.Label>Kommentar:</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={5}
                        value={updatedComment}
                        onChange={(e) => setUpdatedComment(e.target.value)}
                        readOnly={!allowEdit} // Bearbeitung deaktiviert, wenn allowEdit false ist
                    />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                {allowEdit && (
                    <Button variant="secondary" onClick={handleClose}>
                        Abbrechen
                    </Button>
                )}
                {onSave && allowEdit && (
                    <Button variant="primary" onClick={handleSave}>
                        Speichern
                    </Button>
                )}
            </Modal.Footer>
        </Modal>
    );
};

export default CommentPopup;
