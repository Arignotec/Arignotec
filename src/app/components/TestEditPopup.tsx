import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export interface Test {
    date: Date;
    name: string;
    notes: string;
}

const TestEditPopup: React.FC<{
    show: boolean;
    onClose: () => void;
    onSave: (updatedTest: Partial<Test>) => void;
    initialTest: Test;
}> = ({ show, onClose, onSave, initialTest }) => {
    const [test, setTest] = useState(initialTest);

    const handleSave = () => {
        onSave(test);
    };

    return (
        <Modal show={show} onHide={onClose}>
            <Modal.Header closeButton>
                <Modal.Title>Test bearbeiten</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type="text"
                        value={test.name}
                        onChange={(e) => setTest({ ...test, name: e.target.value })}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Datum</Form.Label>
                    <br />
                    <DatePicker
                        selected={test.date}
                        onChange={(date: Date | null) => {
                            if (date) {
                                setTest({ ...test, date });
                            }
                        }}
                        dateFormat="dd/MM/yyyy"
                        className="form-control"
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Anmerkungen</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={3}
                        value={test.notes}
                        onChange={(e) => setTest({ ...test, notes: e.target.value })}
                    />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onClose}>
                    Abbrechen
                </Button>
                <Button variant="primary" onClick={handleSave}>
                    Speichern
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default TestEditPopup;
