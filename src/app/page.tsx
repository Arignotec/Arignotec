"use client";
import { useState } from "react";
import NavbarFrontPage from "@/app/components/navbarFrontPage";
import "./login.css";

const users = [
    { username: "trausner.luk", password: "12345", role: "student" },
    { username: "test.teacher", password: "12345", role: "teacher" }
];

export default function Home() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const handleLogin = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const user = users.find(user => user.username === username && user.password === password);

        if (user) {
            if (user.role === "student") {
                window.location.href = "/student";
            } else if (user.role === "teacher") {
                window.location.href = "/teacher";
            }
        } else {
            setError("Invalid username or password");
        }
    };
    return (
        <>
            <NavbarFrontPage />
            <main>
                <div className="login-card">
                    <div className="card-header">
                        <div className="log">Login</div>
                    </div>
                    <form onSubmit={handleLogin}>
                        <div className="form-group">
                            <label htmlFor="username">Username:</label>
                            <input
                                name="username"
                                id="username"
                                type="text"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <input
                                name="password"
                                id="password"
                                type="password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        {error && <div style={{ color: "red" }}>{error}</div>}
                        <div className="form-group">
                            <input value="Login" type="submit" />
                        </div>
                    </form>
                </div>
            </main>
        </>
    );
}
