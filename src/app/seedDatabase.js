"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var client_1 = require("@prisma/client");
var prisma = new client_1.PrismaClient();
// Method to generate UUID
function generateUUID() {
    // Example implementation, replace with actual UUID generation logic if needed
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0;
        var v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}
//generate hashes
var bcrypt = require('bcryptjs');
function hashString(plainText) {
    return __awaiter(this, void 0, void 0, function () {
        var salt, hashedString, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 3, , 4]);
                    return [4 /*yield*/, bcrypt.genSalt(10)];
                case 1:
                    salt = _a.sent();
                    return [4 /*yield*/, bcrypt.hash(plainText, salt)];
                case 2:
                    hashedString = _a.sent();
                    return [2 /*return*/, hashedString];
                case 3:
                    error_1 = _a.sent();
                    console.error('Error hashing string:', error_1);
                    throw error_1;
                case 4: return [2 /*return*/];
            }
        });
    });
}
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var schoolData, createdSchool, subjectData, subjectPromises, createdSubjects, classesData, classPromises, createdClasses, teachersData, _a, teacherPromises, createdTeachers, classSubjectTeacherPromises, createdClassSubjectTeachers, studentData, _b, students, studentGradesData, error_2;
        var _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
        return __generator(this, function (_r) {
            switch (_r.label) {
                case 0:
                    _r.trys.push([0, 21, 22, 24]);
                    schoolData = {
                        name: 'HTL-Wels',
                    };
                    return [4 /*yield*/, prisma.school.create({
                            data: schoolData,
                        })];
                case 1:
                    createdSchool = _r.sent();
                    subjectData = [
                        {
                            id: generateUUID(),
                            subjectName: 'Softwareentwicklung',
                        },
                        {
                            id: generateUUID(),
                            subjectName: 'Medientechnik',
                        },
                        {
                            id: generateUUID(),
                            subjectName: 'Informationstechnische Projekte',
                        },
                    ];
                    subjectPromises = subjectData.map(function (subject) {
                        return prisma.subject.create({
                            data: subject,
                        });
                    });
                    return [4 /*yield*/, Promise.all(subjectPromises)];
                case 2:
                    createdSubjects = _r.sent();
                    classesData = [
                        {
                            id: generateUUID(),
                            schoolClassName: '3CHIT',
                            schoolId: createdSchool.id,
                        },
                        {
                            id: generateUUID(),
                            schoolClassName: '4BHIT',
                            schoolId: createdSchool.id,
                        },
                    ];
                    classPromises = classesData.map(function (classData) {
                        return prisma.schoolClass.create({
                            data: classData,
                        });
                    });
                    return [4 /*yield*/, Promise.all(classPromises)];
                case 3:
                    createdClasses = _r.sent();
                    _c = {
                        id: generateUUID(),
                        firstName: 'Manfred',
                        lastName: 'Widmann',
                        email: 'manfred.widmann@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("HTL12345")];
                case 4:
                    _a = [
                        (_c.password = _r.sent(),
                            _c.schoolId = createdSchool.id,
                            _c)
                    ];
                    _d = {
                        id: generateUUID(),
                        firstName: 'Jane',
                        lastName: 'Smith',
                        email: 'jane.smith@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("JanESecure")];
                case 5:
                    _a = _a.concat([
                        (_d.password = _r.sent(),
                            _d.schoolId = createdSchool.id,
                            _d)
                    ]);
                    _e = {
                        id: generateUUID(),
                        firstName: 'Michael',
                        lastName: 'Mayer',
                        email: 'michael.mayer@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("michaeLMayer")];
                case 6:
                    teachersData = _a.concat([
                        (_e.password = _r.sent(),
                            _e.schoolId = createdSchool.id,
                            _e)
                    ]);
                    teacherPromises = teachersData.map(function (teacherData) {
                        return prisma.teacher.create({
                            data: teacherData,
                        });
                    });
                    return [4 /*yield*/, Promise.all(teacherPromises)];
                case 7:
                    createdTeachers = _r.sent();
                    classSubjectTeacherPromises = [
                        {
                            id: generateUUID(),
                            classId: createdClasses[0].id,
                            subjectId: createdSubjects[0].id,
                            teacherId: createdTeachers[0].id
                        },
                        {
                            id: generateUUID(),
                            classId: createdClasses[0].id,
                            subjectId: createdSubjects[1].id,
                            teacherId: createdTeachers[1].id
                        },
                        {
                            id: generateUUID(),
                            classId: createdClasses[0].id,
                            subjectId: createdSubjects[2].id,
                            teacherId: createdTeachers[0].id
                        },
                        {
                            id: generateUUID(),
                            classId: createdClasses[1].id,
                            subjectId: createdSubjects[0].id,
                            teacherId: createdTeachers[2].id
                        },
                        {
                            id: generateUUID(),
                            classId: createdClasses[1].id,
                            subjectId: createdSubjects[1].id,
                            teacherId: createdTeachers[1].id
                        },
                        {
                            id: generateUUID(),
                            classId: createdClasses[1].id,
                            subjectId: createdSubjects[2].id,
                            teacherId: createdTeachers[2].id
                        },
                    ].map(function (classSubjectTeacherData) {
                        return prisma.classSubjectTeacher.create({
                            data: classSubjectTeacherData,
                        });
                    });
                    return [4 /*yield*/, Promise.all(classSubjectTeacherPromises)];
                case 8:
                    createdClassSubjectTeachers = _r.sent();
                    _f = {
                        id: generateUUID(),
                        firstName: 'Lukas',
                        lastName: 'Trausner',
                        email: 'lukas.trausner@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("Trausi2")];
                case 9:
                    _b = [
                        (_f.password = _r.sent(),
                            _f.classId = createdClasses[0].id,
                            _f)
                    ];
                    _g = {
                        id: generateUUID(),
                        firstName: 'Eren',
                        lastName: 'Turgut',
                        email: 'eren.turgut@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("Uhcakip10")];
                case 10:
                    _b = _b.concat([
                        (_g.password = _r.sent(),
                            _g.classId = createdClasses[0].id,
                            _g)
                    ]);
                    _h = {
                        id: generateUUID(),
                        firstName: 'Daniel',
                        lastName: 'Ueberbacher',
                        email: 'daniel.ueberbacher@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("SecUrePassWoRd.!")];
                case 11:
                    _b = _b.concat([
                        (_h.password = _r.sent(),
                            _h.classId = createdClasses[0].id,
                            _h)
                    ]);
                    _j = {
                        id: generateUUID(),
                        firstName: 'Student4',
                        lastName: 'Doe',
                        email: 'student4@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password4")];
                case 12:
                    _b = _b.concat([
                        (_j.password = _r.sent(),
                            _j.classId = createdClasses[0].id,
                            _j)
                    ]);
                    _k = {
                        id: generateUUID(),
                        firstName: 'Student5',
                        lastName: 'Doe',
                        email: 'student5@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password5")];
                case 13:
                    _b = _b.concat([
                        (_k.password = _r.sent(),
                            _k.classId = createdClasses[0].id,
                            _k)
                    ]);
                    _l = {
                        id: generateUUID(),
                        firstName: 'Student6',
                        lastName: 'Doe',
                        email: 'student6@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password6")];
                case 14:
                    _b = _b.concat([
                        (_l.password = _r.sent(),
                            _l.classId = createdClasses[1].id,
                            _l)
                    ]);
                    _m = {
                        id: generateUUID(),
                        firstName: 'Student7',
                        lastName: 'Doe',
                        email: 'student7@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password7")];
                case 15:
                    _b = _b.concat([
                        (_m.password = _r.sent(),
                            _m.classId = createdClasses[1].id,
                            _m)
                    ]);
                    _o = {
                        id: generateUUID(),
                        firstName: 'Student8',
                        lastName: 'Doe',
                        email: 'student8@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password8")];
                case 16:
                    _b = _b.concat([
                        (_o.password = _r.sent(),
                            _o.classId = createdClasses[1].id,
                            _o)
                    ]);
                    _p = {
                        id: generateUUID(),
                        firstName: 'Student9',
                        lastName: 'Doe',
                        email: 'student9@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password9")];
                case 17:
                    _b = _b.concat([
                        (_p.password = _r.sent(),
                            _p.classId = createdClasses[1].id,
                            _p)
                    ]);
                    _q = {
                        id: generateUUID(),
                        firstName: 'Student10',
                        lastName: 'Doe',
                        email: 'student10@htl-wels.at'
                    };
                    return [4 /*yield*/, hashString("password10")];
                case 18:
                    studentData = _b.concat([
                        (_q.password = _r.sent(),
                            _q.classId = createdClasses[1].id,
                            _q)
                    ]);
                    return [4 /*yield*/, prisma.student.createMany({
                            data: studentData,
                        })];
                case 19:
                    students = _r.sent();
                    studentGradesData = [
                        {
                            studentId: studentData[0].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[0].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[0].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[1].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[1].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[1].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[2].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[2].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[2].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[3].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[3].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[3].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[4].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[4].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[4].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[5].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[5].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[5].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[6].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[6].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[6].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[7].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[7].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[7].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[8].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[8].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[8].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[9].id,
                            subjectId: subjectData[0].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[9].id,
                            subjectId: subjectData[1].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                        {
                            studentId: studentData[9].id,
                            subjectId: subjectData[2].id,
                            grade: (Math.floor(Math.random() * 5) + 1).toString(), // Random grade between 1 and 5
                            comment: '-',
                        },
                    ];
                    return [4 /*yield*/, prisma.studentGrade.createMany({
                            data: studentGradesData,
                        })];
                case 20:
                    _r.sent();
                    console.log('Database seeding completed successfully.');
                    return [3 /*break*/, 24];
                case 21:
                    error_2 = _r.sent();
                    console.error('Error seeding database:', error_2);
                    return [3 /*break*/, 24];
                case 22: return [4 /*yield*/, prisma.$disconnect()];
                case 23:
                    _r.sent();
                    return [7 /*endfinally*/];
                case 24: return [2 /*return*/];
            }
        });
    });
}
main();
