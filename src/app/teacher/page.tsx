"use client";

import React, { useState } from 'react';
import { Table, Dropdown, Button, ButtonGroup, Modal, Form } from 'react-bootstrap';
import NavbarTeacher from "@/app/components/navbarTeacher";
import CommentPopup from '../components/CommentPopup';
import TestEditPopup from '../components/TestEditPopup';

interface StudentGrade {
    firstName: string;
    lastName: string;
    grade: string;
    comment: string;
}

interface Test {
    date: Date;
    name: string;
    comment: string;
}

interface ClassData {
    subjects: string[];
    tests: { [key: string]: Test[] };
    grades: { [key: string]: StudentGrade[] };
}

const Home: React.FC = () => {
    const initialData: { [key: string]: ClassData } = {
        '3CHIT': {
            subjects: ['Medienttechnik', 'Softwareentwicklung'],
            tests: {
                'Medienttechnik': [
                    { date: new Date('2024-04-14'), name: 'Video Technik', comment: 'Buch S. 123 - 165' },
                    { date: new Date('2023-02-06'), name: 'PHP-PLF', comment: 'Skriptum S. 15 - 24' },
                ],
                'Softwareentwicklung': [
                    { date: new Date('2023-12-03'), name: 'Streams-PLF', comment: 'Skriptum S. 10 - 20' },
                    { date: new Date('2024-02-15'), name: 'JavaFX', comment: 'Skriptum S. 20 - 33' },
                ]
            },
            grades: {
                'Medienttechnik': [
                    { firstName: 'Eren', lastName: 'Turgut', grade: '5', comment: 'Mit viel Glück ne 4-' },
                    { firstName: 'Lukas', lastName: 'Trausner', grade: '4', comment: 'Nächster Test wird vllt. besser...' }
                ],
                'Softwareentwicklung': [
                    { firstName: 'Eren', lastName: 'Turgut', grade: '1', comment: 'Sehr gut gemacht!' },
                    { firstName: 'Lukas', lastName: 'Trausner', grade: '3', comment: 'Verbesserungspotenzial vorhanden.' },
                ]
            }
        },
        '3BHIT': {
            subjects: ['Medientechnik', 'Softwareentwicklung'],
            tests: {
                'Medientechnik': [
                    { date: new Date('2023-02-06'), name: 'PHP-PLF', comment: 'Skriptum S. 15 - 24' },
                    { date: new Date('2024-04-14'), name: 'PHP Skript', comment: 'www3schools' },
                    { date: new Date('2024-04-14'), name: 'Java Skript', comment: 'Buch S. 25 - 56' },
                    { date: new Date('2024-04-14'), name: "Login", comment: 'siehe PHP PLF' },
                    { date: new Date('2024-04-14'), name: 'Überraschung', comment: 'Hihi' },
                    { date: new Date('2024-04-14'), name: 'Webframeworks', comment: 'Online' },
                    { date: new Date('2024-04-14'), name: 'Java', comment: 'Heml' },
                    { date: new Date('2024-04-14'), name: 'Miau', comment: "Miau" },
                ],
                'Softwareentwicklung': [
                    { date: new Date('2023-12-03'), name: 'Streams-PLF', comment: 'Skriptum S. 10-20' },
                    { date: new Date('2024-02-15'), name: 'JavaFX', comment: 'Skriptum S. 20-33' },
                ]
            },
            grades: {
                'Medientechnik': [
                    { firstName: 'Richard', lastName: 'Kuti', grade: '4', comment: 'Gibt noch eine Verbesserungsmöglichkeit' },
                    { firstName: 'Daniel', lastName: 'Überbacher', grade: '1', comment: 'Brav' },
                ],
                'Softwareentwicklung': [
                    { firstName: 'Richard', lastName: 'Kuti', grade: '1', comment: 'Sehr gut gemacht!' },
                    { firstName: 'Daniel', lastName: 'Überbacher', grade: '2', comment: 'Leider 1,2% zu wenig für ein Sehr Gut...' },
                ]
            }
        },
        '4AHIT': {
            subjects: ['Medientechnik'],
            tests: {
                'Medientechnik': [
                    { date: new Date('2023-11-15'), name: 'Implementierung Vorstellung', comment: 'Eigenes Projekt' },
                    { date: new Date('2024-01-28'), name: 'USE-Case PLF', comment: 'Skriptum S. 20-33' },
                ],
            },
            grades: {
                'Medientechnik': [
                    { firstName: 'Julia', lastName: 'Weiß', grade: '2', comment: 'Gut gemacht.' },
                    { firstName: 'Thomas', lastName: 'Koch', grade: '3', comment: 'Kann sich verbessern.' },
                ]
            }
        },
        '2BHIT': {
            subjects: ['Softwareentwicklung'],
            tests: {
                'Softwareentwicklung': [
                    { date: new Date('2023-11-15'), name: 'PLF Funktionen', comment: 'Skriptum S. 16-30' },
                    { date: new Date('2024-01-28'), name: 'Grammatik', comment: 'Kapitel 4-6' },
                ]
            },
            grades: {
                'Softwareentwicklung': [
                    { firstName: 'Felix', lastName: 'Schäfer', grade: '5', comment: 'Deutliche Verbesserung nötig.' },
                    { firstName: 'Jonas', lastName: 'Keller', grade: '3', comment: 'Solide Leistung.' },
                ]
            }
        }
    };

    const [selectedClass, setSelectedClass] = useState('3CHIT');
    const [selectedSubject, setSelectedSubject] = useState<string | undefined>(initialData['3CHIT'].subjects[0]);
    const [data, setData] = useState(initialData);

    const [showAllTests, setShowAllTests] = useState(false);
    const [showGradeCommentPopup, setShowGradeCommentPopup] = useState(false);
    const [selectedComment, setSelectedComment] = useState('');

    const [showEditTestPopup, setShowEditTestPopup] = useState(false);
    const [selectedTest, setSelectedTest] = useState<Test | null>(null);

    const handleGradeChange = (className: string, subject: string, index: number, grade: string) => {
        const newData = { ...data };
        newData[className].grades[subject][index].grade = grade;
        setData(newData);
    };

    const handleGradeCommentClick = (comment: string) => {
        setSelectedComment(comment);
        setShowGradeCommentPopup(true);
    };

    const handleSaveComment = (updatedComment: string) => {
        const updatedData = { ...data };
        const currentGrade = updatedData[selectedClass].grades[selectedSubject!].find(
            (student) => student.comment === selectedComment
        );
        if (currentGrade) {
            currentGrade.comment = updatedComment;
            setData(updatedData);
        }
        setShowGradeCommentPopup(false);
    };

    const handleEditTest = (testIndex: number) => {
        const selectedTest = data[selectedClass].tests[selectedSubject!][testIndex];
        setSelectedTest(selectedTest);
        setShowEditTestPopup(true);
    };

    const handleSaveTest = (updatedTest: Partial<Test>) => {
        if (selectedTest) {
            const updatedData = { ...data };
            updatedData[selectedClass].tests[selectedSubject!].push({
                ...updatedTest,
                date: new Date() // Beispiel: Datum hinzufügen, falls nicht im Formular gesetzt
            } as Test);
            setData(updatedData);
            setShowEditTestPopup(false);
        }
    };

    const classOptions = Object.keys(data);

    const handleClassSelection = (className: string) => {
        setSelectedClass(className);
        setSelectedSubject(data[className].subjects[0]);
        setShowAllTests(false);
    };

    const handleSubjectChange = (subject: string) => {
        setSelectedSubject(subject);
    };

    const toggleShowAllTests = () => {
        setShowAllTests(!showAllTests);
    };

    const toggleShowLessTests = () => {
        setShowAllTests(false);
    };

    return (
        <main>
            <NavbarTeacher />
            <div className="container-fluid mt-4">
                <div className="row">
                    <div className="col-2">
                        <h3>Klassen</h3>
                        <ul className="list-group">
                            {classOptions.map((className, index) => (
                                <li
                                    key={index}
                                    className={`list-group-item ${selectedClass === className ? 'active' : ''}`}
                                    onClick={() => handleClassSelection(className)}
                                >
                                    {className}
                                </li>
                            ))}
                        </ul>
                        <h3>Fach</h3>
                        <Dropdown>
                            <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                {selectedSubject}
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                {data[selectedClass].subjects.map((subject, index) => (
                                    <Dropdown.Item
                                        key={index}
                                        onClick={() => handleSubjectChange(subject)}
                                    >
                                        {subject}
                                    </Dropdown.Item>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <div className="col">
                        <h2>Tests {selectedClass} - {selectedSubject}</h2>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Datum</th>
                                <th>Name</th>
                                <th>Anmerkungen</th>
                                <th>Aktionen</th>
                            </tr>
                            </thead>
                            <tbody>
                            {data[selectedClass].tests[selectedSubject!]?.slice(0, showAllTests ? undefined : 7).map((test, index) => (
                                <tr key={index}>
                                    <td>{test.date.toLocaleDateString()}</td>
                                    <td>{test.name}</td>
                                    <td>{test.comment}</td>
                                    <td>
                                        <Button variant="primary" onClick={() => handleEditTest(index)}>
                                            Bearbeiten
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>

                        {!showAllTests && (
                            <Button variant="secondary" onClick={toggleShowAllTests}>
                                Mehr Tests anzeigen
                            </Button>
                        )}

                        {showAllTests && (
                            <Button variant="secondary" onClick={toggleShowLessTests}>
                                Weniger anzeigen
                            </Button>
                        )}

                        <Button variant="success" className="ms-1" onClick={() => setShowEditTestPopup(true)}>
                            Test hinzufügen
                        </Button>

                        <h2 className="mt-4">Noten {selectedClass} - {selectedSubject}</h2>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Vorname</th>
                                <th>Nachname</th>
                                <th>Note</th>
                                <th>Anmerkung</th>
                                <th>Aktionen</th>
                            </tr>
                            </thead>
                            <tbody>
                            {data[selectedClass].grades[selectedSubject!].map((student, index) => (
                                <tr key={index}>
                                    <td>{student.firstName}</td>
                                    <td>{student.lastName}</td>
                                    <td>
                                        <Dropdown as={ButtonGroup}>
                                            <Button variant="light">{student.grade}</Button>
                                            <Dropdown.Toggle split variant="light" id={`dropdown-split-${index}`} />
                                            <Dropdown.Menu>
                                                {[1, 2, 3, 4, 5, 'Nicht beurteilt'].map((grade, i) => (
                                                    <Dropdown.Item
                                                        key={i}
                                                        onClick={() =>
                                                            handleGradeChange(
                                                                selectedClass,
                                                                selectedSubject!,
                                                                index,
                                                                grade.toString()
                                                            )
                                                        }
                                                    >
                                                        {grade}
                                                    </Dropdown.Item>
                                                ))}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </td>
                                    <td>
                                        {student.comment.length > 30 ? (
                                            <>
                                                {student.comment.substr(0, 30).concat("...")}{' '}
                                            </>
                                        ) : (
                                            student.comment
                                        )}
                                    </td>
                                    <td>
                                        <Button
                                            variant="primary"
                                            onClick={() => handleGradeCommentClick(student.comment)}
                                        >
                                            Bearbeiten
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>

            {showGradeCommentPopup && (
                <CommentPopup
                    comment={selectedComment}
                    onSave={handleSaveComment}
                    onClose={() => setShowGradeCommentPopup(false)}
                />
            )}

            {showEditTestPopup && (
                <TestEditPopup
                    show={showEditTestPopup}
                    onClose={() => setShowEditTestPopup(false)}
                    onSave={handleSaveTest}
                    initialTest={{ date: new Date(), name: '', notes: '' }}
                />
            )}
        </main>
    );
};

export default Home;
