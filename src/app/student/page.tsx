"use server";
import React from 'react';
import NavbarStudent from "@/app/components/navbarStudent";
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const bcrypt = require('bcryptjs');

async function comparePasswords(plainText: string, hashedPassword: string) {
    try {
        const isMatch = await bcrypt.compare(plainText, hashedPassword);
        return isMatch;
    } catch (error) {
        console.error('Error comparing passwords:', error);
        throw error;
    }
}

export async function Home() {
    //eren.turgut@htl-wels.at
    //Uhcakip10
    const currentStudentEmail = "lukas.trausner@htl-wels.at";
    const currentStudentPassword = "Trausi2";

    const currentStudent = await prisma.student.findUnique({
        where: {
            email: currentStudentEmail,
        },
        select: {
            class: {
                select: {
                    classSubjectTeachers: {
                        select: {
                            teacher: {
                                select: {
                                    firstName: true,
                                    lastName: true
                                }
                            },
                            subject: {
                                select: {
                                    subjectName: true,
                                    id: true,
                                }
                            }
                        }
                    }
                }
            },
            studentGrades: {
                select: {
                    grade: true,
                    comment: true,
                    subjectId: true
                }
            },
            password: true,
            firstName: true,
            lastName: true,
        }
    });
    if (currentStudent == null) {
        console.log('Student does not exist!');
    }else{
        comparePasswords(currentStudentPassword, currentStudent.password)
            .then(isMatch => {
                if (isMatch) {
                    console.log('Passwords match!'); //andere aktion
                } else {
                    console.log('Passwords do not match!'); //andere aktion
                }
            })
            .catch(err => console.error('Error:', err));
        // Map subjects and teachers
        const classData = currentStudent.class.classSubjectTeachers.map(subjectTeacher => {
            const subjectName = subjectTeacher.subject.subjectName;
            const teacherName = `${subjectTeacher.teacher.firstName} ${subjectTeacher.teacher.lastName}`;
            const subjectId = subjectTeacher.subject.id;

            // Find the corresponding grade for the subject
            const gradeData = currentStudent.studentGrades.find(grade => grade.subjectId === subjectId);
            const grade = gradeData ? gradeData.grade : '-';
            const comment = gradeData ? gradeData.comment : '-';

            return {
                subjectName,
                teacherName,
                grade,
                comment
            };
        });

        const HomeComponent = () => {
            return (
                <main>
                    <NavbarStudent />
                    <div className="container mt-5">
                        <div className="table-responsive">
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th colSpan={4}>
                                        Angemeldet als: {currentStudent.firstName} {currentStudent.lastName}
                                    </th>
                                </tr>
                                <tr>
                                    <th scope="col">Fach</th>
                                    <th scope="col">Lehrer</th>
                                    <th scope="col">Note</th>
                                    <th scope="col">Anmerkung</th>
                                </tr>
                                </thead>
                                <tbody>
                                {classData.map((data, index) => (
                                    <tr key={index}>
                                        <td>{data.subjectName}</td>
                                        <td>{data.teacherName}</td>
                                        <td>{data.grade}</td>
                                        <td>{data.comment}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </main>
            );
        };
        return <HomeComponent />;
    }

}
export default Home;
